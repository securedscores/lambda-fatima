module.exports = {
  ethereumcolumbusPOD1: {
    expire: 1523591999, // Thursday, April 12, 2018 11:59:59 PM GMT-04:00
    claim: {
      "Attended Proof of Drink #2!": {
        event: "Proof of Drink 4/12",
        date: "April 12, 2018",
        location: "Zeno's"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  rsmclaim1: {
    expire: 1546257600, //Monday, December 31, 2018 4:00:00 AM GMT-08:00
    claim: {
      "Attended the RSM Conference": {
        event:
          "How Blockchain and Cryptocurrency Work & How They Will Disrupt Client Business (71482)",
        date: "May 1, 2018",
        location: "United States"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  rsmclaim2: {
    expire: 1546257600, //Monday, December 31, 2018 4:00:00 AM GMT-08:00
    claim: {
      "Attended the RSM Conference": {
        event:
          "How Blockchain and Cryptocurrency Work & How They Will Disrupt Client Business (71482)",
        date: "May 23, 2018",
        location: "United States"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  dchack: {
    expire: 1520362800, //Tuesday, 6 de March de 2018 19:00:00 GMT // 19:00 EST
    claim: {
      "Attended Discover Blockchain Technology Code-a-Thon 2018": {
        event: "Discover Blockchain Technology Code-a-Thon 2018",
        date: "March 6, 2018",
        location:
          "Georgetown University McDonough School of Business, Washington, DC"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  dcsummit: {
    expire: 1520571600, //Friday, 9 de March de 2018 5:00:00 GMT // 00:00 EST
    claim: {
      "Attended DC Blockchain Summit": {
        event: "DC Blockchain Summit",
        date: "March 7-8, 2018",
        location:
          "Georgetown University McDonough School of Business, Washington, DC"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethereumjoburg: {
    expire: 1520460000, // Wednesday, 7th of March, 22:00:00 GMT 23:00:00 South Africa
    claim: {
      "Attended Ethereum Meetup Johannesburg": {
        event: "Ethereum Meetup Johannesburg",
        date: "March 7, 2018",
        location: "Impact Hub Johannesburg"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  blockchainafrica: {
    expire: 1520719200000, // Friday, 9th of March, 22:00:00 GMT 23:00:00 South Africa
    claim: {
      "Attended Blockchain Africa 2018": {
        event: "Blockchain Africa 2018",
        date: "March 8-9, 2018",
        location: "Microsoft South Africa"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethereumcapetown: {
    expire: 1520719200, // Saturday, 10th of March, 22:00:00 GMT 23:00:00 South Africa
    claim: {
      "Attended Ethereum Meetup Cape Town": {
        event: "Ethereum Meetup Cape Town",
        date: "March 10, 2018",
        location: "Impact Hub Cape Town"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethcc2018: {
    expire: 1520528400, // Thursday, 8 de March de 2018 17:00:00 GMT 18:00:00 France
    claim: {
      "Attended EthCC": {
        event: "Ethereum Community Conference",
        date: "March 8-10, 2018",
        location: "Paris, France"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  uportCommunityCall3: {
    expire: 1522771524, // Tuesday, April 3, 2018 12:05:24 PM GMT-04:00 DST
    claim: {
      "Attended uPort Community Call": {
        event: "uPort Community Call #3",
        date: "April 3, 2018",
        location: "uPort Town Hall"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethereumnairobi: {
    expire: 1521748800, // Thursday, 22nd of March, 20:00:00 GMT 23:00:00 Kenya
    claim: {
      "Attended Ethereum Meetup Nairobi": {
        event: "Ethereum Meetup Nairobi",
        date: "March 22, 2018",
        location: "Metta Nairobi"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethereumabuja: {
    expire: 1521928800, // Saturday, 24th of March, 22:00:00 GMT 23:00:00 Nigeria
    claim: {
      "Attended Ethereum Meetup Abuja": {
        event: "Ethereum Meetup Abuja",
        date: "March 24, 2018",
        location: "Civic Innovation Hub Abuja"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  },
  ethereumlagos: {
    expire: 1522188000, // Tuesday, 27th of March, 22:00:00 GMT 23:00:00 Nigeria
    claim: {
      "Attended Ethereum Meetup Lagos": {
        event: "Ethereum Meetup Lagos",
        date: "March 27, 2018",
        location: "Impact Hub Lagos"
      }
    },
    signer_name: "uPort Team",
    signer_mnid: "2oiSESCx5Y7oMHDAxx9iUBkz88rdA9yvmW3"
  }
};
